FROM alpine:edge

RUN apk add --no-cache \
    build-base \
    openssl-dev \
    zlib-dev \
    bats kyua \
    doas \
    fakeroot \
    git \
    openssl \
    rsync \
    scdoc \
    tar \
    cmd:getcap \
    && adduser -D buildozer \
    && mkdir -p /workdir \
    && chown buildozer: /workdir

COPY overlay /

RUN chmod 0600 /etc/doas.d/doas.conf

# For abuild-keygen
ENV SUDO=doas

USER buildozer

ENTRYPOINT ["/entrypoint"]
